/**
 *  GradientKey.js
 *
 *
 */

import { interpolateViridis } from 'd3-scale-chromatic';

let canvas = document.getElementById('feedback-gradient'),
    ctx = canvas.getContext('2d'),
    maxX = 220,
    maxY = 20,
    gradient = null;

gradient = ctx.createLinearGradient(0,0, maxX, 0);

let currentColor = '';

// Add three color stops
for (let i = 0; i <= 10; i++) {
  currentColor = interpolateViridis(i/10);
  gradient.addColorStop(i/10, currentColor);
}

ctx.fillStyle = gradient;
ctx.fillRect(0, 0, canvas.width, canvas.height);

function init() {
  updatePointer();
}

function updatePointer() {
  let body = document.getElementsByTagName('body')[0],
      currentValue = parseInt(body.dataset.currentValue, 10),
      pointer = document.getElementById('feedback-gradient-pointer');
  if (currentValue > 0) {
    pointer.style.left = currentValue + '%';
  }
  window.requestAnimationFrame(updatePointer);
}

init();