/**
 *
 *  index.js
 *
 *
 */

import axios from 'axios';
import mapboxgl from '!mapbox-gl';
import { hsl } from 'd3-color';
import {
  interpolatePlasma,
  interpolateGreens,
  interpolateViridis
} from 'd3-scale-chromatic';
import '../scss/index.scss';
import './GradientKey';

let body = document.getElementsByTagName('body')[0];

mapboxgl.accessToken = body.dataset.mapboxAccessToken;

let colorsSet = false,
    hoveredCountryId = null,
    initialRenderDelay = null,
    map = null,
    ppiMin = 0,
    ppiMax = 0,
    testProceedInt = null;

let LOAD_STATUS = {
      countries: false,
      countryData: false
    },
    INIT = {
      countries: [],
      countryData: [],
      countryFilters: ['all'],
      iso_3166_1_disputed: [
        'D0AEIR1',
        'D0AEIR2',
        'D0BRUY1',
        'D0BTCN1',
        'D0BTCN2',
        'D0CNIN0',
        'D0CNIN1',
        'D0CNIN2',
        'D0CNIN3',
        'D0CNIN4',
        'D0CNIN5',
        'D0CNIN6',
        'D0CNIN7',
        'D0CNIN8',
        'D0CNIN9',
        'D0CNPHTWVN1',
        'D0CNTWVN1',
        'D0EGSD1',
        'D0EGSD2',
        'D0INNP1',
        'D0INNP2',
        'D0INPK1',
        'D0INPK2',
        'D0JPRU1',
        'D0KESS1',
        'D0KESS2',
        'D0ILSY1',
        'D0SDSS1',
        'D0SDSS2',
        'D0SESS3',
        'D0KESS4'
      ],
      worldviews: ['JP', 'CN', 'IN']
    };

INIT.worldviews.forEach((item) => {
  INIT.countryFilters.push(['!=', 'worldview', item]);
});
INIT.iso_3166_1_disputed.forEach((item) => {
  INIT.countryFilters.push(['!=', 'iso_3166_1', item]);
});

// INIT.countryFilters.push(['==', 'iso_3166_1', 'LS']);
// INIT.countryFilters.push(['==', 'worldview', 'US']);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

function init() {
  body.setAttribute('data-current-value', 0);
  testProceedInt = setInterval(() => {
    testProceed();
  }, 500);
  initData1();
  initData2();
}

function initData1() {
  axios.get('./data/countries.json')
    .then((d) => {
      INIT.countries = [...d.data];
      console.log('Loaded ' + d.data.length + ' countries.');
      // console.log(INIT.countries);
      LOAD_STATUS.countries= true;
    })
    .catch(err => console.log(err));
}

function initData2() {
  axios.get('./data/cost_of_living.json')
    .then((d) => {
      INIT.countryData = [...d.data.countries];
      // console.log(INIT.countryData);
      LOAD_STATUS.countryData = true;
    })
    .catch(err => console.log(err));
}

function filterCountries() {
  // console.log(INIT.countries);
  INIT.countries.forEach((item) => {
    if (item.countryDisplay === false) {
      // console.log(item.countryId);
      INIT.countryFilters.push(['!=', 'iso_3166_1', item.countryId.toUpperCase()]);
    }
  });
  // console.log(INIT.countryFilters);
  LOAD_STATUS.countryData = true;
}

function setPPIMinMax() {
  let sortedCountries = [];
  sortedCountries = INIT.countryData.sort((a, b) => {
    return (a.purchasing_power_index > b.purchasing_power_index) ? 1 : -1;
  });
  ppiMin = sortedCountries[0].purchasing_power_index;
  ppiMax = sortedCountries[sortedCountries.length - 1].purchasing_power_index;
}

function showCountries() {
  let detected = [],
      detectedCount = 0;
  // console.log(INIT.countries);
  // console.log(INIT.countryData);
  INIT.countries.forEach((c2) => {
    c2.countryDisplay = false;
  });
  INIT.countryData = INIT.countryData.sort((a, b) => {
    return (a.countryId > b.countryId) ? 1 : -1;
  });
  // console.log('Sorted:');
  // console.log(INIT.countryData);
  // console.log('End sorted.')
  INIT.countryData.forEach((c1) => {
    INIT.countries.forEach((c2) => {
      if (c1.countryId === c2.countryId) {
        detectedCount++;
        c2.countryDisplay = true;
      }
    });
  });
  // console.log('countries: ' + INIT.countries.length);
  // console.log('countryData: ' + INIT.countryData.length);
  // console.log('country data detectedCount: ' + detectedCount);
  // console.log(INIT.countries);
  // filterCountries();
}

function testProceed() {
  // console.log(LOAD_STATUS.countries);
  // console.log(LOAD_STATUS.countryData);
  if ((LOAD_STATUS.countries === true) &&
     (LOAD_STATUS.countryData === true)) {
    clearInterval(testProceedInt);
    setPPIMinMax();
    showCountries();
    initMap();
  }
}

function initMap() {
  map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/reddeleon/ckmv1zx45040p17mr2fmyi3on',
    center: [0, 30],
    zoom: 1
  });
  map.dragRotate.disable();
  // map.touchZoomRotate.disableRotation();
  bindMainHandlers();
}

function addLayer() {
  map.addLayer({
    'id': 'country-boundaries',
    'type': 'fill',
    'source': 'countries',
    'source-layer': 'country_boundaries',
    'layout': {},
    'paint': {
      // 'fill-outline-color': 'rgba(255, 0, 0, 1)',
      // 'fill-color': 'rgba(255, 0, 0, 1)'
      'fill-outline-color': 'rgba(0, 0, 0, 0)',
      'fill-color': 'rgba(0, 0, 0, 0)'
    }
  });
}

function addColoredLayer() {
  if (map.getLayer('country-boundaries-colored')) {
     map.removeLayer('country-boundaries-colored');
  }
  map.addLayer({
    'id': 'country-boundaries-colored',
    'type': 'fill',
    'source': 'countries',
    'source-layer': 'country_boundaries',
    'layout': {},
    'paint': {
      'fill-outline-color': 'rgba(0, 0, 0, 0)',
      'fill-color': [
        'case',
        ['boolean', ['feature-state', 'hover'], false],
        ['string', ['feature-state', 'new-color-hover'], '#cccccc'], // hover
        ['string', ['feature-state', 'new-color'], '#cccccc']
      ]
    }
  });
}

function getColorFromRange(rawValue, minVal, maxVal) {
  let rangeMax = maxVal - minVal,
      scaleColor = interpolateViridis(rawValue / rangeMax);
  return (rawValue !== null) ? scaleColor : 0xffffff;
}

function addSource() {
  map.addSource('countries', {
    type: 'vector',
    url: 'mapbox://mapbox.country-boundaries-v1'
  });
}

function setFilter() {
  map.setFilter('country-boundaries',
    INIT.countryFilters
  );
}

function setColoredFilter() {
  map.setFilter('country-boundaries-colored',
    INIT.countryFilters
  );
}

function bindMouseMoveHandler() {
  map
    .off('mousemove', () => {})
    .on('mousemove', 'country-boundaries', function (e) {
      if (e.features.length > 0) {
        console.log(e.features[0]);
        showCurrent(e.features[0]);
        map.getCanvas().style.cursor = 'default';
        body.dataset.currentValue = ((e.features[0].state.ppi/ppiMax) * 100) || 0;
        if (hoveredCountryId !== null) {
          map.setFeatureState(
            {
              'source': 'countries',
              'sourceLayer': 'country_boundaries',
              'id': hoveredCountryId
            },
            { hover: false }
          );
        }
        hoveredCountryId = e.features[0].id;
        map.setFeatureState(
          { 
            'source': 'countries',
            'sourceLayer': 'country_boundaries',
            'id': hoveredCountryId
          },
          { hover: true }
        );
      }
    });
}

function bindMouseLeaveHandler() {
  map
    .off('mouseleave', () => {})
    .on('mouseleave', 'country-boundaries', function () {
      map.getCanvas().style.cursor = 'grab';
      if (hoveredCountryId !== null) {
        map.setFeatureState(
          {
            'source': 'countries',
            'sourceLayer': 'country_boundaries',
            'id': hoveredCountryId
          },
          { hover: false }
        );
      }
      hoveredCountryId = null;
    });
}

function bindMainHandlers() {
  map.on('load', () => {
    addSource();
    addLayer();
    initialRenderDelay = setTimeout(() => {
      colorsSet = false;
      setFilter();
      render();
    }, 500);
  });
  map.on('zoomend', () => {
    colorsSet = false;
    render();
  });
  // console.log('ppiMin: ' + ppiMin);
  // console.log('ppiMax: ' + ppiMax);
}

function render() {
  setColors();
  addColoredLayer();
  setColoredFilter();
  bindMouseMoveHandler();
  bindMouseLeaveHandler();
}

function showCurrent(country) {
  let feedback = document.getElementById('feedback-text');
  // feedback.innerHTML = 'Zoom: ' + map.getZoom();
  feedback.innerHTML = `<h4>${country.properties.name_en}</h4>`;
  if (!country.state.ppi || country.state.ppi === 0) {
    feedback.innerHTML += `Purchasing Power Index: ?`;
  } else {
    feedback.innerHTML += `Purchasing Power Index: ${country.state.ppi}`;
  }
}

function setColors() {
  let cList = [];
  console.log('Rendered countries: ' + 
    (map.queryRenderedFeatures({ layers: ['country-boundaries'] })).length
  );
  // console.log('colorsSet: ' + colorsSet);
  if (colorsSet === false) {
    colorsSet = true;
    let features = map.queryRenderedFeatures({ layers: ['country-boundaries'] });
    console.log('features.length: ' + features.length);
    features.forEach((f, i) => {
      // console.log(f);
      // console.log(
      //   f.properties.name_en + ' : ' + 
      //   f.properties.iso_3166_1 + ' : '
      // );
      // console.log('countryId: ' + f.properties.iso_3166_1.toLowerCase());

      // console.log(f.properties.iso_3166_1);
      // console.log(f.properties.iso_3166_1 + ' :: ' + f.properties.name_en);
      // console.log(getPPI(f.properties.iso_3166_1.toLowerCase()));
      let countryId = f.properties.iso_3166_1.toLowerCase(),
          newColor = getColorFromRange(getPPI(countryId), ppiMin, ppiMax),
          newColorHover = hsl(newColor).brighter(0.5),
          thisPPI = getPPI(countryId);
      if (thisPPI === 0) {
        newColor = '#cccccc';
        newColorHover = '#dddddd';
      } else {
        newColorHover = newColorHover.toString();
      }
      map.setFeatureState({
        'source': 'countries',
        'sourceLayer': 'country_boundaries',
        'id': f.id
      }, {
        'new-color': newColor,
        'new-color-hover': newColorHover,
        'ppi': thisPPI
      });
    });
  }
}

function getPPI(countryId) {
  let ppi = 0;
  INIT.countryData.forEach((c) => {
    if (c.countryId === countryId) {
      ppi = c.purchasing_power_index;
    }
  });
  return ppi;
}

init();