Cost of living and purchasing power related to average income
=============================================================

[https://www.worlddata.info/cost-of-living.php](https://www.worlddata.info/cost-of-living.php)

We ajusted the average cost of living inside the USA (based on 2019 and 2020) to an index of 100. All other countries are related to this index. Therefore with an index of e.g. 80, the usual expenses in another country are 20% less then inside the United States.

The monthly income (please do not confuse it with a wage or salary) is calculated from the gross national income per capita.

The calculated purchasing power index is again based on a value of 100 for the United States. If it is higher, people can afford more based on the cost of living in relation to income. If it is lower, so the population is less wealthy.

The example of Switzerland:
With a cost of living index of 129 all goods are on average about 29 percent more expensive than in the USA. But the average income in Switzerland of 7125 USD is also 30% higher, which means that citizens can also afford more goods. Now you calculate the 29% higher costs against the 30% higher income. In the result, people in Switzerland can still afford about 0 percent more than a US citizen.

