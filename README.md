mapbox-ppi
==========

## Overview

This is a custom demo of Mapbox Countries for assessing the zoom interface UX conditions presented by the extreme range of country sizes throughout the world (n.b., Macao, Hong Kong, etc.). This demo was created to assess a revamp of [fcpamap.com](http://fcpamap.com/) and what a redesign might entail if a more robust, higher-definition mapping system like Mapbox is used.

A new proof-of-concept is introduced here, using a dynamic gradient key as an improvement on fixed gradients. This proposes the use of D3's [d3-scale-chromatic](https://github.com/d3/d3-scale-chromatic) library.

A live version of this site can be viewed here: [mapbox-ppi.red-deleon.com](http://mapbox-ppi.red-deleon.com/)

